import glob
import os
import importlib
import types

# Base class for brain cell models
#import braincellmodel
from braincellmodel import BrainCellModel, MultiCellModel

from beeler_reuter_1977 import Beeler_reuter_1977
from fitzhughnagumo_manual import FitzHughNagumoManual
from nocellmodel import NoCellModel
from rogers_mcculloch_manual import RogersMcCulloch
from tentusscher_2004_mcell import Tentusscher_2004_mcell
from tentusscher_panfilov_2006_epi_cell import Tentusscher_panfilov_2006_epi_cell
from fenton_karma_1998_BR_altered import Fenton_karma_1998_BR_altered
from fenton_karma_1998_MLR1_altered import Fenton_karma_1998_MLR1_altered
from grandi_pasqualini_bers_2010 import Grandi_pasqualini_bers_2010
from adex import AdExManual

# Only add supported cell model here if it is tested to actually run
# with some multistage discretization

supported_cell_models = (adex)

# Iterate over modules and collect BrainCellModels
#supported_cell_models = set()
#all_names = set()

# Get absolut path to module
#module_dir = os.sep.join(os.path.abspath(braincellmodel.__file__).split(os.sep)[:-1])
# for module_path in glob.glob(os.path.join(module_dir, "*.py")):
#     module_str = os.path.basename(module_path)[:-3]
#     if module_str in ["__init__", "braincellmodel"]:
#         continue
#     module = importlib.import_module("cbcbrain.cellmodels."+module_str)
#     for name, attr in module.__dict__.items():
#         if isinstance(attr, types.ClassType) and issubclass(attr, BrainCellModel):
#             supported_cell_models.add(attr)
#             globals()[name] = attr
#             all_names.add(name)

# # Remove base class
# supported_cell_models.remove(BrainCellModel)
# supported_cell_models = tuple(supported_cell_models)
# All BrainCellModel names
#__all__ = [str(s) for s in supported_cell_models]
#__all__.append("supported_cell_models")
