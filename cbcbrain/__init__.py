"""
The cbcbrain Python module is a problem and solver collection for
cardiac electrophysiology models.

To import the module, type::

  from cbcbrain import *

"""

import dolfinimport

# Import all of dolfin with possibly dolfin-adjoint on top
from dolfinimport import *

# Model imports
from cbcbrain.brainmodels import BrainModel
from cbcbrain.cellmodels import *
from cbcbrain.markerwisefield import *

# Solver imports
from cbcbrain.splittingsolver import BasicSplittingSolver
from cbcbrain.splittingsolver import SplittingSolver
from cbcbrain.cellsolver import BasicSingleCellSolver, SingleCellSolver
from cbcbrain.cellsolver import BasicBrainODESolver, BrainODESolver
from cbcbrain.bidomainsolver import BasicBidomainSolver
from cbcbrain.bidomainsolver import BidomainSolver
from cbcbrain.monodomainsolver import BasicMonodomainSolver
from cbcbrain.monodomainsolver import MonodomainSolver

# Various utility functions, mainly for internal use
import cbcbrain.utils

# NB: Workaround for FEniCS 1.7.0dev
import ufl
ufl.algorithms.apply_derivatives.CONDITIONAL_WORKAROUND = True

# Set-up some global parameters
beat_parameters = dolfinimport.Parameters("beat-parameters")
beat_parameters.add("enable_adjoint", True)
