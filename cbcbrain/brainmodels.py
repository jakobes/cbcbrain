"""This module contains a container class for brain models:
:py:class:`~cbcbrain.brainmodels.BrainModel`.  This class
should be instantiated for setting up specific brain simulation
scenarios.
"""

# Copyright (C) 2012 Marie E. Rognes (meg@simula.no)
# Use and modify at will
# Last changed: 2016-04-21

__all__ = ["BrainModel"]

from dolfinimport import Parameters, Mesh, Constant, GenericFunction, error, Expression, \
    MeshFunction, CellFunction, FacetFunction
from markerwisefield import Markerwise, handle_markerwise
from cellmodels import *

# ------------------------------------------------------------------------------
# Brain models
# ------------------------------------------------------------------------------

class BrainModel(object):
    """
    A container class for brain models. Objects of this class
    represent a specific brain simulation set-up and should provide

    * A computational domain
    * A brain cell model
    * Intra-cellular and extra-cellular conductivities
    * Various forms of stimulus (optional).

    This container class is designed for use with the splitting
    solvers (:py:mod:`cbcbrain.splittingsolver`), see their
    documentation for more information on how the attributes are
    interpreted in that context.

    *Arguments*
      domain (:py:class:`dolfin.Mesh`)
        the computational domain in space
      time (:py:class:`dolfin.Constant` or None )
        A constant holding the current time.
      M_i (:py:class:`ufl.Expr`)
        the intra-cellular conductivity as an ufl Expression
      M_e (:py:class:`ufl.Expr`)
        the extra-cellular conductivity as an ufl Expression
      cell_models (:py:class:`~cbcbrain.cellmodels.braincellmodel.BrainCellModel`)
        a cell model or a dict with cell models associated with a cell model domain
      stimulus (:py:class:`dict`, optional)
        A typically time-dependent external stimulus given as a dict,
        with domain markers as the key and a
        :py:class:`dolfin.Expression` as values. NB: it is assumed
        that the time dependence of I_s is encoded via the 'time'
        Constant.
      applied_current (:py:class:`ufl.Expr`, optional)
        an applied current as an ufl Expression

    """
    def __init__(self, domain, time, M_i, M_e, cell_models,
                 stimulus=None, applied_current=None, cell_domains=None, facet_domains=None):
        "Create BrainModel from given input."
        self._handle_input(domain, time, M_i, M_e, cell_models,
                           stimulus, applied_current, cell_domains, facet_domains)

    def _handle_input(self, domain, time, M_i, M_e, cell_models,
                      stimulus=None, applied_current=None, cell_domains=None, facet_domains=None):
        """ TODO: Eventually, I want to change Markerwise for two fields called cell_domains
        and facet_domains, to make sure that the Measures in the solver are consistent
        """

        # Check input and store attributes
        msg = "Expecting domain to be a Mesh instance, not %r" % domain
        assert isinstance(domain, Mesh), msg
        self._domain = domain

        msg = "Expecting time to be a Constant instance, not %r." % time
        assert isinstance(time, Constant) or time is None, msg
        self._time = time

        # Assume cell_domain and facet_domains are Mesh/Facet/cell_functions
        if cell_domains is None:
            cell_domains = CellFunction("size_t", domain)
            cell_domains.set_all(0)

        if facet_domains is None:
            facet_domains = FacetFunction("size_t", domain)
            facet_domains.set_all(0)


        if isinstance(M_i, dict) and isinstance(M_e, dict):
            msg = "Expecting M_i and M_e to have same keys"
            assert set(M_i.keys()) == set(M_e.keys()), msg   # Check keys are equal
            # TODO: Use duck typing?
            #msg = "Expecting M_k.values() to be Expressions, k = i, e"
            #assert all(map(lambda x: isinstance(x, Expression), M_i.values() + M_e.values())), msg
        else:
            # TODO: Use duck typing?
            #msg = "Expecting M_k to be Expression, k = i, e"
            #assert all(map(lambda x: isinstance(x, Expression), [M_i, M_e])), msg
            M_i = {0 : M_i}      # Keys are the same as in default cell_domains
            M_e = {0 : M_e}

        assert set(M_i.keys()) == set(M_e.keys()) == set(cell_domains.array())

        self._cell_domains = cell_domains       # For varying e.g. conductivities
        self._facet_domains = facet_domains     # For applying external stimulus

        self._intracellular_conductivity = M_i
        self._extracellular_conductivity = M_e

        # Handle cell_models
        self._cell_models = cell_models

        # Handle stimulus
        self._stimulus = handle_markerwise(stimulus, GenericFunction)   # TODO: Add check

        # Handle applied current
        ac = applied_current
        # TODO: Add check
        self._applied_current = handle_markerwise(ac, GenericFunction)

    def cell_domains(self):
        "Return the physical regions of the brain."
        return self._cell_domains

    def facet_domains(self):
        "Return the physical surfaces of the brain"
        return self._facet_domains

    def applied_current(self):
        "An applied current: used as a source in the elliptic bidomain equation."
        return self._applied_current

    def stimulus(self):
        "A stimulus: used as a source in the parabolic bidomain equation"
        return self._stimulus

    def conductivities(self):
        """Return the intracellular and extracellular conductivities
        as a tuple of UFL Expressions.

        *Returns*
        (M_i, M_e) (:py:class:`tuple` of :py:class:`ufl.Expr`)
        """
        return (self.intracellular_conductivity(),
                self.extracellular_conductivity())

    def intracellular_conductivity(self):
        #"The intracellular conductivity (:py:class:`ufl.Expr`)."
        "The intracellular conductivity (:py:class:`Markerwise`)."
        return self._intracellular_conductivity

    def extracellular_conductivity(self):
        "The intracellular conductivity (:py:class:`Markerwise`)."
        return self._extracellular_conductivity

    def time(self):
        "The current time (:py:class:`dolfin.Constant` or None)."
        return self._time

    def domain(self):
        "The spatial domain (:py:class:`dolfin.Mesh`)."
        return self._domain

    def cell_models(self):
        "Return the cell models"
        return self._cell_models
